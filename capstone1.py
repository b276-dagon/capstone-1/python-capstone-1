from abc import ABC


class Person():
    def get_full_name(self):
        pass
    def add_request(self):
        pass
    def check_request(self):
        pass
    def add_user(self):
        pass

class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
    # For Getter and setter 
    def get_first_name(self):
        print(f'{self._first_name}')
    def set_first_name(self, first_name):
        self._first_name = first_name
    def get_last_name(self):
        f'{self._last_name}'
    def set_last_name(self, last_name):
        self._last_name = last_name
    def get_email(self):
        f'{self._email}'
    def set_email(self, email):
        self._email = email
    def get_department(self):
        f'{self._department}'
    def set_department(self, deparment):
        self._department = deparment
    
    # methods
    def get_full_name(self):
       return f'{self._first_name} {self._last_name}'
    def check_request(self):
        return f'Request has been checking'
    def add_request(self):
        return f'Request has been added'
    def add_user(self):
        return f'User has been added'
    def login(self):
        return f'{self._email} has logged in'
    def logout(self):
        return f'{self._email} has logged out'
    

class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = []
        
    # For Getter and setter 
    def get_first_name(self):
        f'{self._first_name}'
    def set_first_name(self, first_name):
        self._first_name = first_name
    def get_last_name(self):
        f'{self._last_name}'
    def set_last_name(self, last_name):
        self._last_name = last_name
    def get_email(self):
        f'{self._email}'
    def set_email(self, email):
        self._email = email
    def get_department(self):
        return f'{self._department}'
    def set_department(self, deparment):
        self._department = deparment

    # Methods
    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'
    def check_request(self):
        return f'Request has been checking'
    def add_user(self):
        return f'User has been added'
    def login(self):
        return f'{self._email} has logged in'
    def logout(self):
        return f'{self._email} has logged out'
    def add_member(self , member):
        self._members.append(member)
    def get_members(self):
        return(f'{self._members}')
        
class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
    # For Getter and setter 
    def get_first_name(self):
        f'{self._first_name}'
    def set_first_name(self, first_name):
        self._first_name = first_name
    def get_last_name(self):
        f'{self._last_name}'
    def set_last_name(self, last_name):
        self._last_name = last_name
    def get_email(self):
        f'{self._email}'
    def set_email(self, email):
        self._email = email
    def get_department(self):
        f'{self._department}'
    def set_department(self, deparment):
        self._department = deparment

    # Methods
    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'
    def check_request(self):
        return f'Request has been checking'
    def add_user(self):
        return f'User has been added'
    def login(self):
        return f'{self._email} has logged in'
    def logout(self):
        return f'{self._email} has logged out'
    def add_user(self):
        return f'User has been added'
   

class Request():
    def __init__(self, name, requester , date_requested):
        self._name = name
        self._requester = requester
        self._date_requested = date_requested
        


    def update_request(self, name, requester, date_quested):
        self._name = name
        self._requester = requester
        self._date_requested = date_quested
    def get_status(self):
        print(f'Status of request {self._name} : {self._status}')
    def set_status(self, status):
        self._status = status
    def close_request(self):
        return f'Request {self._name} has been closed'
    def cancel_request(self):
        return f'Request {self._name} has been cancelled'



employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")


admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.add_request() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.add_member(employee3)
teamLead1.add_member(employee4)
for indiv_emp in teamLead1._members:
    print(indiv_emp.get_full_name())

assert admin1.add_user() == "User has been added"
req2.set_status("closed")
print(req2.close_request())


